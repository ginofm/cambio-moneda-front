import { CambioMonedaOutput } from './../_model/cambioMonedaOutput';
import { CambioMonedaInput } from './../_model/cambioMonedaInput';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CambioMonedaService {

  private urlEndPoint: string = `${environment.HOST}/cambioMoneda`;

  constructor(private http: HttpClient, private router: Router) { 
    
  }

  ejecutarCambio(cambioMonedaInput: CambioMonedaInput): Observable<CambioMonedaOutput> {

    console.log(cambioMonedaInput);
    return this.http.post(this.urlEndPoint, cambioMonedaInput).pipe(
        map((response: any) => response.cambioMoneda as CambioMonedaOutput),
        catchError(e => {
            if (e.status == 400) {
                return throwError(e);
            }

            if (e.error.mensaje) {
                console.error(e.error.mensaje);
            }

            return throwError(e);
        })
    )
}
}

import { CambioMonedaOutput } from './../../_model/cambioMonedaOutput';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cambio-output',
  templateUrl: './cambio-output.component.html',
  styleUrls: ['./cambio-output.component.css']
})
export class CambioOutputComponent implements OnInit {

  @Input() cambioMonedaOutput: CambioMonedaOutput;

  constructor() { }

  ngOnInit(): void {
  }

}

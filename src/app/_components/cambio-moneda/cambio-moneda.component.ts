import { CambioMonedaInput } from './../../_model/cambioMonedaInput';
import { CambioMonedaOutput } from './../../_model/cambioMonedaOutput';
import { CambioMonedaService } from './../../_service/cambio-moneda.service';
import { Component, Inject, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cambio-moneda',
  templateUrl: './cambio-moneda.component.html',
  styleUrls: ['./cambio-moneda.component.css']
})
export class CambioMonedaComponent implements OnInit {

  cambioMonedaSalida : CambioMonedaOutput;
  procesoEjecutado : boolean=false;
  cambioMonedaInput : CambioMonedaInput = new CambioMonedaInput();


  constructor(
    private cambioMonedaService : CambioMonedaService) { }

  ngOnInit(): void {

  }

  realizarCambio(): void{
    this.cambioMonedaInput.monedaDestino = this.cambioMonedaInput.monedaDestino.toLowerCase();
    this.cambioMonedaInput.monedaOrigen = this.cambioMonedaInput.monedaOrigen.toLowerCase();
    this.cambioMonedaService.ejecutarCambio(this.cambioMonedaInput)
    .subscribe(cambioMoneda => { cambioMoneda 
      console.log("retorno");
      console.log(cambioMoneda);
      this.cambioMonedaSalida = cambioMoneda;
      Swal.fire('Nueva Historia',`Monto base: ${cambioMoneda.monto}. Monto cambiado: ${cambioMoneda.montoCambio}`,'success');
      this.procesoEjecutado =true;
    }
    )
  }

}

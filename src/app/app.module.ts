import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CambioMonedaComponent } from './_components/cambio-moneda/cambio-moneda.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CambioOutputComponent } from './_components/cambio-output/cambio-output.component';

const routes: Routes = [
  { path: 'cambioMonedaLink', component: CambioMonedaComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    CambioMonedaComponent,
    CambioOutputComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NoopAnimationsModule,
    FormsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

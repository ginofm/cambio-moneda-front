export class CambioMonedaInput{
    montoBase: number;
    monedaDestino: string;
    monedaOrigen: string;
}
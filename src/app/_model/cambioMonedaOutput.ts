export class CambioMonedaOutput {
    monto:number;
    montoCambio:number;
    monedaOrigen: string;
    monedaDestino: string;
    tipoCambio: number;

}